Fenom
=====
The Fenom integration for the Yii framework

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Add

```
"y2i/yii2-fenom": "*"
```
and
```
    "repositories":[
        {
            "type":"package",
            "package":{
                "name":"yiisoft/yii2-fenom",
                "version":"1.0",
                "source":{
                    "type":"git",
                    "url":"https://bitbucket.org/HiCMS/yii2-fenom.git",
                    "reference":"master"
                },
                "autoload":{
                    "psr-4": {
			               "yii\\fenom\\": ""
	                 }
                }
            }
        }
    ]
```

to the require section of your `composer.json` file.

Add

```
        'view' => [
            'renderers' => [
                'tpl' => [ //'twig' => [
                    'class' => 'yii\fenom\ViewRenderer',
                    'templatePath' => __DIR__ .'/../views/site/fenom',
                    'compilePath' => __DIR__ .'/../views/site/fenom/cache',
                    'options' => [
                        'force_compile' => true,
                        'strip' => true,
                    ],
                ],
            ],
        ],
```

to the require section of your Yii config (in `components` section) file.

Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
        return $this->render('fenom/about.tpl', [
            'citys'=>[
               ['id'=>'1','name'=>'Kiev'],
               ['id'=>'2','name'=>'Paris']
            ]
        ]);
```

`views\site\fenom\about.tpl`

```html
<h1>Fenom</h1>
<ul>
    {foreach $citys as $city}
        <li><a href="#{$city.id}">{$city.name}</a></li>
    {foreachelse}
        <li>Empty</li>
    {/foreach}
</ul>
```